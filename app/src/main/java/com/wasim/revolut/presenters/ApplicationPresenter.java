package com.wasim.revolut.presenters;

import com.wasim.revolut.elements.ActivityContract;
import com.wasim.revolut.elements.ApiService;
import com.wasim.revolut.models.RatesModel;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ApplicationPresenter implements ActivityContract.Presenter {

    private ApiService apiService;
    private ActivityContract.View mView;

    @Inject
    public ApplicationPresenter(ApiService apiService, ActivityContract.View mView) {
        this.apiService = apiService;
        this.mView = mView;
    }

    @Override
    public void loadData(String base) {

        apiService.getRates(base)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<RatesModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(RatesModel ratesModel) {
                        if (ratesModel != null) {
                            mView.dataLoadedSuccessfully(ratesModel);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.dataError("Error", e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                    }
                });


    }

}
