package com.wasim.revolut.modules;

import com.wasim.revolut.elements.ActivityContract;
import com.wasim.revolut.elements.ActivityScope;
import com.wasim.revolut.elements.ApiService;
import com.wasim.revolut.presenters.ApplicationPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityMvpModule {

    private ActivityContract.View mView;

    public ActivityMvpModule(ActivityContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @ActivityScope
    ActivityContract.View provideView() {
        return mView;
    }

    @Provides
    @ActivityScope
    ApplicationPresenter providePresenter(ApiService apiService, ActivityContract.View mView) {
        return new ApplicationPresenter(apiService, mView);
    }

}
