package com.wasim.revolut.modules;

import android.content.Context;

import com.wasim.revolut.MainActivity;
import com.wasim.revolut.elements.ActivityContext;
import com.wasim.revolut.elements.ActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityContextModule {
    private MainActivity mainActivity;
    private Context context;

    public ActivityContextModule(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        context = mainActivity;
    }

    @Provides
    @ActivityScope
    public MainActivity providesActivity() {
        return mainActivity;
    }

    @Provides
    @ActivityScope
    @ActivityContext
    public Context providesContext() {
        return context;
    }

}
