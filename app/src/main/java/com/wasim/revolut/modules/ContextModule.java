package com.wasim.revolut.modules;

import android.content.Context;

import com.wasim.revolut.elements.ApplicationContext;
import com.wasim.revolut.elements.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {
    private Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    @ApplicationScope
    @ApplicationContext
    public Context providesContext() {
        return context;
    }
}
