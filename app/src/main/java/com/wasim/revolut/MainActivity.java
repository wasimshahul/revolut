package com.wasim.revolut;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.treebo.internetavailabilitychecker.InternetAvailabilityChecker;
import com.treebo.internetavailabilitychecker.InternetConnectivityListener;
import com.wasim.revolut.Utility.AppDialogs;
import com.wasim.revolut.Utility.Constants;
import com.wasim.revolut.adapters.RatesAdapter;
import com.wasim.revolut.elements.ActivityComponent;
import com.wasim.revolut.elements.ActivityContext;
import com.wasim.revolut.elements.ActivityContract;
import com.wasim.revolut.elements.ApplicationComponent;
import com.wasim.revolut.elements.ApplicationContext;
import com.wasim.revolut.elements.DaggerActivityComponent;
import com.wasim.revolut.models.RateModel;
import com.wasim.revolut.models.Rates;
import com.wasim.revolut.models.RatesModel;
import com.wasim.revolut.modules.ActivityContextModule;
import com.wasim.revolut.modules.ActivityMvpModule;
import com.wasim.revolut.presenters.ApplicationPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements ActivityContract.View, InternetConnectivityListener {

    @BindView(R.id.ratesRV)
    RecyclerView ratesRV;

    RatesAdapter ratesAdapter;
    ArrayList<RateModel> rateModels;

    RateModel baseRateModel;
    public int userInput = 1;

    InternetAvailabilityChecker mInternetAvailabilityChecker;
    boolean hasNetwork = false;

    ActivityComponent activityComponent;
    @Inject
    @ApplicationContext
    public Context context;

    @Inject
    @ActivityContext
    public Context activityContext;

    @Inject
    ApplicationPresenter applicationPresenter;

    ApplicationComponent applicationComponent;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Inject
    AppDialogs appDialogs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        initiateView();
        initPreFabs();
    }

    private void initiateView() {
        ratesRV = (RecyclerView) findViewById(R.id.ratesRV);
        rateModels = new ArrayList<>();
        ratesAdapter = new RatesAdapter(rateModels);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        ratesRV.setLayoutManager(mLayoutManager);
        ratesRV.setItemAnimator(new DefaultItemAnimator());
        ratesRV.setAdapter(ratesAdapter);
    }

    private void initPreFabs() {

        //Initiate Internet checker
        InternetAvailabilityChecker.init(this);
        mInternetAvailabilityChecker = InternetAvailabilityChecker.getInstance();
        mInternetAvailabilityChecker.addInternetConnectivityListener(this);

        applicationComponent = RevolutApplication.get(this).applicationComponent;

        activityComponent = DaggerActivityComponent.builder()
                .activityContextModule(new ActivityContextModule(this))
                .activityMvpModule(new ActivityMvpModule(this))
                .applicationComponent(applicationComponent)
                .build();
        activityComponent.injectActivity(this);

        //Set base currency
        setBase(Constants.DEFAULT_CURRENCY_NAME, Constants.DEFAULT_CURRENCY_VALUE);
        setUserInput(Integer.parseInt(Constants.DEFAULT_CURRENCY_VALUE));
        initiateApiCall();
    }

    @Override
    public void dataLoadedSuccessfully(RatesModel ratesModel) {

        hideProgress();
        try {
            if (rateModels.get(0).currencyName.equals(baseRateModel.currencyName)) {
            } else {
                ratesAdapter.notifyItemChanged(0, baseRateModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        rateModels.clear();
        rateModels.add(getBaseRateModel());
        setCurrencyList(ratesModel);
    }

    @Override
    public void dataError(String call, String statusMessage) {

    }

    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    public void refreshCalculatedCurrencies() {
        ArrayList<RateModel> refreshedCurrencyList = new ArrayList<>(getCurrencyList());
        rateModels.clear();
        for (int i = 1; i <= refreshedCurrencyList.size(); i++) {
            RateModel rateModel = new RateModel(refreshedCurrencyList.get(i - 1).currencyName, refreshedCurrencyList.get(i - 1).currencyValue);
            rateModels.add(rateModel);
            try {
                ratesAdapter.notifyItemChanged(i, rateModel);
            } catch (Exception e) {
            }
        }
    }

    private ArrayList<RateModel> getCurrencyList() {
        return rateModels;
    }

    private void setCurrencyList(RatesModel ratesModel) {

        String[] rates = new Gson().toJson(ratesModel.rates, Rates.class).split(",");

        for (int i = 1; i <= rates.length; i++) {
            String[] currency = rates[i - 1].split(":");
            RateModel rateModel = new RateModel(currency[0], currency[1]);
            rateModels.add(rateModel);
            try {
                ratesAdapter.notifyItemChanged(i, rateModel);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setBase(String currencyName, String currencyValue) {
        baseRateModel = new RateModel(currencyName, currencyValue);
        applicationPresenter.loadData(currencyName);
    }

    public RateModel getBaseRateModel() {
        if (baseRateModel == null) {
            setBase(Constants.DEFAULT_CURRENCY_NAME, Constants.DEFAULT_CURRENCY_VALUE);
        }
        return baseRateModel;
    }

    public int getUserInput() {
        return userInput;
    }

    public void setUserInput(int value) {
        if ((value <= 0)) {
            userInput = 0;
        } else {
            userInput = value;
        }
    }

    public void scrollListToTop() {
        ratesRV.scrollToPosition(0);
    }

    public void initiateApiCall() {
        Handler handler = new Handler();
        int delay = Constants.API_REFRESH_RATE_MILLISECS; //milliseconds

        handler.postDelayed(new Runnable() {
            public void run() {
                if (hasNetwork) {
                    applicationPresenter.loadData(baseRateModel.currencyName);
                }
                ;
                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        hasNetwork = isConnected;
        if (!isConnected) {
            appDialogs.showBottomAlertDialog(MainActivity.this);
        } else {
            appDialogs.dismissDialog();
        }
    }
}
