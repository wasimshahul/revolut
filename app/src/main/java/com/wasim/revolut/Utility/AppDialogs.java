package com.wasim.revolut.Utility;

import android.app.Activity;
import android.view.View;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.wasim.revolut.R;

import javax.inject.Inject;

public class AppDialogs {

    private BottomSheetDialog mBottomSheetDialog;

    @Inject
    public AppDialogs() {

    }

    public void showBottomAlertDialog(Activity activity) {
        mBottomSheetDialog = new BottomSheetDialog(activity);
        View sheetView = activity.getLayoutInflater().inflate(R.layout.bottom_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    public void dismissDialog() {
        if (mBottomSheetDialog != null) {
            try {
                mBottomSheetDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
