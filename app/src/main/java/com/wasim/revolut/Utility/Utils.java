package com.wasim.revolut.Utility;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.wasim.revolut.BuildConfig;
import com.wasim.revolut.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class Utils {
    static Toast mToast;
    public static String baseURL = BuildConfig.BASE_URL;

    public static void showToast(Context context, String statusMsg) {
        if (mToast != null) mToast.cancel();
        mToast = Toast.makeText(context, statusMsg, Toast.LENGTH_SHORT);
        mToast.show();
    }

    public static void showLog(String message) {
        if (message != null)
            Log.e("Logged", message);
    }

    public static String loadJSONFromAsset(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("currency_abbreviation.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getCurrencyAbbreviation(Context context, String currency) {

        String abbreviation = "";
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset(context));

            abbreviation = obj.getString(currency);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return abbreviation;
    }

    public static int getFlagForCurrency(String currencyName) {
        int returnValue = 0;
        switch (currencyName) {
            case "AUD":
                returnValue = R.drawable.ic_aud;
                break;
            case "BGN":
                returnValue = R.drawable.ic_bgn;
                break;
            case "BRL":
                returnValue = R.drawable.ic_brl;
                break;
            case "CAD":
                returnValue = R.drawable.ic_cad;
                break;
            case "CHF":
                returnValue = R.drawable.ic_chf;
                break;
            case "CNY":
                returnValue = R.drawable.ic_cny;
                break;
            case "CZK":
                returnValue = R.drawable.ic_czk;
                break;
            case "DKK":
                returnValue = R.drawable.ic_dkk;
                break;
            case "EUR":
                returnValue = R.drawable.ic_eur;
                break;
            case "GBP":
                returnValue = R.drawable.ic_gbp;
                break;
            case "HKD":
                returnValue = R.drawable.ic_hkd;
                break;
            case "HRK":
                returnValue = R.drawable.ic_hrk;
                break;
            case "HUF":
                returnValue = R.drawable.ic_huf;
                break;
            case "IDR":
                returnValue = R.drawable.ic_idr;
                break;
            case "ILS":
                returnValue = R.drawable.ic_ils;
                break;
            case "INR":
                returnValue = R.drawable.ic_ind;
                break;
            case "ISK":
                returnValue = R.drawable.ic_isk;
                break;
            case "JPY":
                returnValue = R.drawable.ic_jpy;
                break;
            case "KRW":
                returnValue = R.drawable.ic_krw;
                break;
            case "MXN":
                returnValue = R.drawable.ic_mxn;
                break;
            case "MYR":
                returnValue = R.drawable.ic_myr;
                break;
            case "NOK":
                returnValue = R.drawable.ic_nok;
                break;
            case "NZD":
                returnValue = R.drawable.ic_nzd;
                break;
            case "PHP":
                returnValue = R.drawable.ic_php;
                break;
            case "PLN":
                returnValue = R.drawable.ic_pln;
                break;
            case "RON":
                returnValue = R.drawable.ic_ron;
                break;
            case "RUB":
                returnValue = R.drawable.ic_rub;
                break;
            case "SEK":
                returnValue = R.drawable.ic_sek;
                break;
            case "SGD":
                returnValue = R.drawable.ic_sgd;
                break;
            case "THB":
                returnValue = R.drawable.ic_thb;
                break;
            case "TRY":
                returnValue = R.drawable.ic_try;
                break;
            case "USD":
                returnValue = R.drawable.ic_usd;
                break;
            case "ZAR":
                returnValue = R.drawable.ic_zar;
                break;
        }
        return returnValue;
    }

    public static String perfectionise(String text) {
        return text.replace("\"", "").replace("{", "").replace("}", "");
    }
}
