package com.wasim.revolut.Utility;


public class Constants {
    public static final String DEFAULT_CURRENCY_NAME = "EUR";
    public static final String DEFAULT_CURRENCY_VALUE = "1";
    public static final int API_REFRESH_RATE_MILLISECS = 1000;
}
