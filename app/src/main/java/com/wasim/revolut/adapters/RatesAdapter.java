package com.wasim.revolut.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wasim.revolut.MainActivity;
import com.wasim.revolut.R;
import com.wasim.revolut.Utility.Constants;
import com.wasim.revolut.Utility.Utils;
import com.wasim.revolut.models.RateModel;

import java.util.List;

public class RatesAdapter extends RecyclerView.Adapter<RatesAdapter.MyViewHolder> {

    private List<RateModel> rateModelList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView currencyNameTV, currencyAbbreviationTV;
        private EditText currencyValueEdT;
        private LinearLayout currencyLL;
        private ImageView flagIV;

        public MyViewHolder(View view) {
            super(view);
            currencyNameTV = (TextView) view.findViewById(R.id.currencyNameTV);
            currencyValueEdT = (EditText) view.findViewById(R.id.currencyValueEdT);
            currencyAbbreviationTV = (TextView) view.findViewById(R.id.currencyAbbreviationTV);
            currencyLL = (LinearLayout) view.findViewById(R.id.currencyLL);
            flagIV = (ImageView) view.findViewById(R.id.flagIV);
        }
    }


    public RatesAdapter(List<RateModel> rateModelList) {
        this.rateModelList = rateModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.currency_row, parent, false);

        context = itemView.getContext();

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RateModel rate = rateModelList.get(position);

        //Perfectionate Currency name & value
        String name = Utils.perfectionise(rate.currencyName);
        String value = Utils.perfectionise(rate.currencyValue);

        holder.currencyNameTV.setText(name);
        Glide.with(context).load(Utils.getFlagForCurrency(name)).into(holder.flagIV);

        if (position == 0) {
            holder.currencyValueEdT.setEnabled(true);
            try {
                holder.currencyValueEdT.setText(((MainActivity) context).getUserInput());
            } catch (Exception e) {
                holder.currencyValueEdT.setText(Constants.DEFAULT_CURRENCY_VALUE);
            }
            holder.currencyValueEdT.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (!holder.currencyValueEdT.getText().toString().isEmpty()) {
                        try {
                            ((MainActivity) context).setUserInput(Integer.parseInt(holder.currencyValueEdT.getText().toString()));
                            ((MainActivity) context).refreshCalculatedCurrencies();
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } else {
            holder.currencyValueEdT.setEnabled(false);
            float val = ((MainActivity) context).getUserInput() * Float.parseFloat(value);
            holder.currencyValueEdT.setText(String.valueOf(val));
            holder.currencyLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    notifyItemMoved(position, 0);
                    ((MainActivity) context).scrollListToTop();
                    ((MainActivity) context).setBase(holder.currencyNameTV.getText().toString(), holder.currencyValueEdT.getText().toString());
                }
            });
        }
        holder.currencyAbbreviationTV.setText(Utils.getCurrencyAbbreviation(context, name));
    }

    @Override
    public int getItemCount() {
        return rateModelList.size();
    }
}