package com.wasim.revolut.elements;

import com.wasim.revolut.models.RatesModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    @GET("latest")
    Observable<RatesModel> getRates(@Query("base") String base);

}
