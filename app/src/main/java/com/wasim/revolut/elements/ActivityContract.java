package com.wasim.revolut.elements;

import com.wasim.revolut.models.RatesModel;

public interface ActivityContract {

    interface View {
        void dataLoadedSuccessfully(RatesModel ratesModel);

        void dataError(String call, String statusMessage);
    }

    interface Presenter {
        void loadData(String base);
    }

}
