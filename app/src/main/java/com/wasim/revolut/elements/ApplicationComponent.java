package com.wasim.revolut.elements;

import android.content.Context;

import com.wasim.revolut.RevolutApplication;
import com.wasim.revolut.modules.ApiModule;
import com.wasim.revolut.modules.ContextModule;

import dagger.Component;

@ApplicationScope
@Component(modules = {ContextModule.class, ApiModule.class})
public interface ApplicationComponent {

    ApiService getApiService();

    @ApplicationContext
    Context getContext();

    void injectApplication(RevolutApplication application);

}
