package com.wasim.revolut.elements;

import android.content.Context;

import com.wasim.revolut.MainActivity;
import com.wasim.revolut.modules.ActivityContextModule;
import com.wasim.revolut.modules.ActivityMvpModule;

import dagger.Component;

@ActivityScope
@Component(modules = {ActivityContextModule.class, ActivityMvpModule.class},
        dependencies = ApplicationComponent.class)
public interface ActivityComponent {

    @ActivityContext
    Context getContext();

    void injectActivity(MainActivity mainActivity);

}