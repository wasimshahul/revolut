
package com.wasim.revolut.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RatesModel {

    @SerializedName("base")
    public String base;

    @SerializedName("date")
    public String date;

    @SerializedName("rates")
    public Rates rates;

}
