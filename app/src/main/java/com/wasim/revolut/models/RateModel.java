package com.wasim.revolut.models;

import com.google.gson.annotations.SerializedName;

public class RateModel {
    @SerializedName("currencyName")
    public String currencyName;

    @SerializedName("currencyValue")
    public String currencyValue;

    public RateModel(String currencyName, String currencyValue) {
        this.currencyName = currencyName;
        this.currencyValue = currencyValue;
    }
}
