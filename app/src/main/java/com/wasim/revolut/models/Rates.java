
package com.wasim.revolut.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rates {

    @SerializedName("AUD")

    public Float aUD;
    @SerializedName("BGN")

    public Float bGN;
    @SerializedName("BRL")

    public Float bRL;
    @SerializedName("CAD")

    public Float cAD;
    @SerializedName("CHF")

    public Float cHF;
    @SerializedName("CNY")

    public Float cNY;
    @SerializedName("CZK")

    public Float cZK;
    @SerializedName("DKK")

    public Float dKK;
    @SerializedName("GBP")

    public Float gBP;
    @SerializedName("HKD")

    public Float hKD;
    @SerializedName("HRK")

    public Float hRK;
    @SerializedName("HUF")

    public Float hUF;
    @SerializedName("IDR")

    public Float iDR;
    @SerializedName("ILS")

    public Float iLS;
    @SerializedName("INR")

    public Float iNR;
    @SerializedName("ISK")

    public Float iSK;
    @SerializedName("JPY")

    public Float jPY;
    @SerializedName("KRW")

    public Float kRW;
    @SerializedName("MXN")

    public Float mXN;
    @SerializedName("MYR")

    public Float mYR;
    @SerializedName("NOK")

    public Float nOK;
    @SerializedName("NZD")

    public Float nZD;
    @SerializedName("PHP")

    public Float pHP;
    @SerializedName("PLN")

    public Float pLN;
    @SerializedName("RON")

    public Float rON;
    @SerializedName("RUB")

    public Float rUB;
    @SerializedName("SEK")

    public Float sEK;
    @SerializedName("SGD")

    public Float sGD;
    @SerializedName("THB")

    public Float tHB;
    @SerializedName("TRY")

    public Float tRY;
    @SerializedName("USD")

    public Float uSD;
    @SerializedName("ZAR")

    public Float zAR;

}
