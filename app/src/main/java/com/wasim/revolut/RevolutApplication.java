package com.wasim.revolut;

import android.app.Activity;
import android.app.Application;

import com.wasim.revolut.elements.ApplicationComponent;
import com.wasim.revolut.elements.DaggerApplicationComponent;
import com.wasim.revolut.modules.ContextModule;

public class RevolutApplication extends Application {

    ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        dependencyInjection();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public static RevolutApplication get(Activity activity) {
        return (RevolutApplication) activity.getApplication();
    }

    private void dependencyInjection() {
        applicationComponent = DaggerApplicationComponent.builder()
                .contextModule(new ContextModule(this)).build();

        applicationComponent.injectApplication(this);
    }

}